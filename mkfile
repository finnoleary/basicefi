CC=gcc
CFLAG= -std=c99 -Wall -Os -fno-stack-protector -fpic -fshort-wchar \
       -mno-red-zone -DEFI_FUNCTION_WRAPPER
CINCL= -I/usr/include/efi/ -I/usr/include/efi/x86_64

LD=ld
LDFLAG= -nostdlib -znocombreloc -T /usr/lib/elf_x86_64_efi.lds
LDLIBS= /usr/lib/crt0-efi-x86_64.o -shared -Bsymbolic -L/usr/lib/ \
        -l:libgnuefi.a -l:libefi.a

OFLAGS= -j .text -j .sdata -j .data -j .dynamic -j .dynsym -j .rel -j rela \
        -j .reloc --target=efi-app-x86_64

OVMF=bin/OVMF-pure-efi.fd
QEMU=qemu-system-x86_64
QFLAGS= -cpu qemu64 -net none -display sdl -bios $OVMF

CFILES= `{ ls src/*.c }
OFILES=${CFILES:src/%.c=bin/%.o}
TARGET= bin/basic.efi
SOTARG=${TARGET:%.efi=%.so}
IMGTAR=${TARGET:%.efi=%.img}

CTEST=test/test.c
OTEST=bin/util.o
TESTBIN=bin/run-tests

do:QV: build img
 :

build: clean
  $CC -c $CFLAG $CINCL $CFILES
  mv *.o bin/
  $LD $OFILES $LDFLAG $LDLIBS -o $SOTARG
  objcopy $OFLAGS $SOTARG $TARGET

img:V: build
  partimg=$(mktemp -p /tmp/)
  dd if=/dev/zero of=$IMGTAR bs=512 count=93750
  parted $IMGTAR -s -a minimal mklabel gpt
  parted $IMGTAR -s -a minimal mkpart EFI FAT16 2048s 93716s
  parted $IMGTAR -s -a minimal toggle 1 boot
  dd if=/dev/zero of=$partimg bs=512 count=91669
  mformat -i $partimg -h 32 -t 32 -n 64 -c 1
  mcopy -i $partimg $TARGET ::
  dd if=$partimg of=$IMGTAR bs=512 count=91669 seek=2048 conv=notrunc

live:V:
  $QEMU $QFLAGS -drive file=$IMGTAR,format=raw,if=ide

buildtests: clean
  $CC -c -DTEST $CFLAG $CINCL $CFILES
  mv *.o bin/

test:V: buildtests
  ./test/mktest.sh $CTEST
  $CC -o $TESTBIN $OTEST $CFLAG $CINCL $CTEST
  ./$TESTBIN

clean:V:
  rm -f $CTEST $TESTBIN bin/*.o $TARGET $SOTARG $IMGTAR
