# get a list of function heads
fnhds=$(grep -h '^void test_.*$' src/*.c | sed -e 's/$/;/' -)
names=$(printf "%s\n" $fnhds | sed -n 's/\(test_.*\)(void);$/\1/p' -)
names_strings=$(echo $names | tr ' ' '\n' | sed -e 's/\(\w*\)/"\1", \1,/' -)

nnames=$(echo $names | tr ' ' '\n' | wc -l)

cat > ./test/test.c << ENDFILE
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

$fnhds

struct {
  const char *name;
  int (*f)(void);
} test_functions[$nnames] = {
  $names_strings
};

int main(int _, char **__)
{
        int i;
        for (i = 0; i < $nnames; i++) {
                printf("-----------------\n");
                printf("Running test %d: %s\n",
                       i, test_functions[i].name);
                test_functions[i].f();
                printf("Finished test %d.\n", i);
        }
}
ENDFILE

