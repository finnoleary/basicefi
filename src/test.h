#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#define UINTN size_t
#define EFIAPI 
#define CHAR16 uint16_t

#define STRINGIFY_(a) #a
#define STRINGIFY(a) STRINGIFY_(a)
