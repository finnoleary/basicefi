#include <efi.h>
#include <efilib.h>

#ifdef TEST 
#include "test.h"
#endif

#include "head.h"

uintn efiapi u_strlen(char16 *s)
{
	uintn n = 0;
	
	while (s && *s++)
		n++;

	return n;
}

char16* efiapi u_strdup(char16 *s)
{
	uintn i = 0;
	uintn n = u_strlen(s)+1;
	char16 *d = NULL;
	if (!s || !(d = malloc((n)*sizeof(char16)))) { return NULL; }

	do {
		d[i] = s[i];
	} while (++i < n);

	return d;
}

int efiapi u_isdigit(char16 c)
{
	return (c > 47 && c < 58);
}

int efiapi u_ctoi(char16 c)
{
	return (c-48);
}

int efiapi u_atoi(char16 *s)
{
	int i = 0;
	int n = 0;
	for (i = 0; s && u_isdigit(s[i]); i++) {
		n *= 10;
		n += u_ctoi(s[i]);
	}

	return n;
}

static int u_ndigits(int i)
{
	int n = 0;
	while (i > 0)
		i /= 10, n++;

	return n;
}

void efiapi memcpy(restrict char *dest, restrict char *src, uintn length)
/* Same semantics as posix memcpy: ensure it's non-overlapping. */
{
	uintn i;
	if (!dest || !src || !length || dest == src) { return; }
	for (i = 0; i < length; i++)
		dest[i] = src[i];
}

void efiapi memmove(char *dest, char *src, uintn length)
{
	if (!dest || !src || !length || src == dest) { return; }
	/* dest is to the left of src and overlapping */
	if (src > dest && src < dest+length) {
		
	}
	/* dest is to the right of src and overlapping */
	else if (dest > src && dest < src+length) {
		
	}
	else {
		memcpy(dest, src, length);
	}
}

#ifdef TEST

#define TESTSTRING     L"HELLO WORLD"
#define TESTSTRINGSIZE 11
void test_strlen(void)
{
	char16 *ts = TESTSTRING;
	uintn sz = TESTSTRINGSIZE;
	uintn sz2 = u_strlen(ts);
	printf("proper size: %lu; given size: %lu;\n", sz,sz2);
	assert(u_strlen(ts) == TESTSTRINGSIZE);
}

void test_strdup(void)
{
	char16 *ts = TESTSTRING;
	char16 *ts2 = u_strdup(ts);
	int i;
	for (i = 0; i < TESTSTRINGSIZE+1; i++) {
		printf("ts[%d] (%c) :: ts2[%d] (%c)\n", i, ts[i], i, ts2[i]);
		assert(ts[i] == ts2[i]);
	}
}

#define TESTNUM_S L"1234221"
#define TESTNUM  1234221
void test_atoi(void)
{
	char16 *nstr = TESTNUM_S;
	int n = TESTNUM;
	int m = u_atoi(nstr);
	printf("input: '%ls'; output: %d; expected: %d\n", nstr, m, n);
	assert((n == m));
}

#define TESTNUMBER 5553235
#define TESTNUMBERLEN 7
void test_ndigits(void)
{
	int n = TESTNUMBER;
	int len = TESTNUMBERLEN;
	int i = u_ndigits(TESTNUMBER);
	printf("proper: %d; given: %d\n", len, i);
	assert(len == i);
}

#endif
