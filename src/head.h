
extern EFI_SYSTEM_TABLE *symtab;
#define call(...) uefi_call_wrapper(__VA_ARGS__)

/* Now my hands are happy :) */
#define char16 CHAR16
#define uintn UINTN
#define efiapi EFIAPI
#define efi_status EFI_STATUS
#define efi_success EFI_SUCCESS
#define efi_event EFI_EVENT
#define efi_input_key EFI_INPUT_KEY

typedef struct {
	char16 filename[32];

	char16 *buffer;
	uintn cursor, bufsize;
} ProgramState;


typedef struct {
	char16 *s;
	uintn i, max;
} Vector;

void efiapi handle_char(ProgramState *S, char16 rune);
void efiapi handle_scancode(ProgramState *S, uintn code);

/* memm */
void* efiapi malloc(uintn size);
void  efiapi free(void *);

/* line stuff? */
void efiapi line_init(ProgramState *);
void efiapi line_insert(ProgramState *, char16);
void efiapi line_delete(ProgramState *);

/* buffer */
void efiapi vec_new

/* util */
uintn efiapi u_strlen(char16 *s);
char16* efiapi u_strdup(char16 *s);
int efiapi u_isdigit(char16 c);
int efiapi u_ctoi(char16 c);
int efiapi u_atoi(char16 *s);
