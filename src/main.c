#include <efi.h>
#include <efilib.h>

#include "head.h"

enum event_loop_signals { STOP, CONTINUE };

void efiapi init(void);
int efiapi event_loop(ProgramState *S);

EFI_SYSTEM_TABLE *symtab = NULL;

efi_status efiapi efi_main(EFI_HANDLE imgh, EFI_SYSTEM_TABLE *st)
{
	ProgramState PS;

	InitializeLib(imgh, st);
	symtab = st;

	init();
	Print(L"Starting Event Loop\r\n");
	int n = 0;
	while ((n = event_loop(&PS)) == CONTINUE)
		;

	return efi_success;
}

void efiapi init(void)
{
	void *Reset = symtab->ConOut->Reset;
	void *OutputString = symtab->ConOut->OutputString;

	call(Reset, 2, symtab->ConOut, 0);
	call(OutputString, 2, symtab->ConOut, L"Loading Interface!\r\n"); 
	return;
}

void efiapi handle_char(ProgramState *S, char16 rune)
{

}

void efiapi handle_scancode(ProgramState *S, uintn scancode)
{
	return;
}

int efiapi event_loop(ProgramState *S)
{
	void *WaitForEvent = symtab->BootServices->WaitForEvent;
	void *ReadKeyStroke = symtab->ConIn->ReadKeyStroke;
	efi_event WaitForKey = symtab->ConIn->WaitForKey;
	efi_status status;
	efi_input_key key;
	uintn index;

	/* Poll event shit here */
	status = call(WaitForEvent, 3, 1, &WaitForKey, &index);
	if (status < 0 || index != 0) {
		Print(L"WaitForEvent status < 0 or index == 0\r\n");
		return CONTINUE;
	}

	status = call(ReadKeyStroke, 2, symtab->ConIn, &key);
	if (status < 0) { return CONTINUE; }

	if (key.UnicodeChar != 0) {
		handle_char(S, key.UnicodeChar);
		return CONTINUE;
	}

	switch (key.ScanCode) {
		case 0x17: /* ESCAPE */
			return STOP;
		default:
			handle_scancode(S, key.ScanCode);
			break;
	}
	return CONTINUE;
}
