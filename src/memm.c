#include <efi.h>
#include <efilib.h>

#include "head.h"

void* efiapi malloc(uintn size)
{
	void *AllocatePool = symtab->BootServices->AllocatePool;
	void *result = NULL;

	if ((call(AllocatePool, 3, EfiLoaderData, size, &result)) < 0) {
		return NULL;
	}
	return result;
}

void efiapi free(void *p)
{
	void *FreePool = symtab->BootServices->FreePool;
	call(FreePool, 1, p);
}
